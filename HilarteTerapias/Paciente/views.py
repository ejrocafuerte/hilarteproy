import subprocess
from django.shortcuts import render
from django.conf import settings
import datetime
from Anamnesis_del_Lenguaje.models import Anamnesis_del_Lenguaje
from Paciente.models import Paciente
from Paciente.models import Discapacidad
from Area_Social.models import Area_Social
from Habilidades_Basicas.models import Habilidades_Basicas
# Create your views here.
def resumen_sistema(request):
    now = datetime.date.today()
    pacientes = Paciente.objects.all()
    listapacientes=[]
    x=0
    t=Paciente.objects.all().count()
    pacientesMotora=Discapacidad.objects.filter(motora__gt=0).exclude(auditiva__gt=0).exclude(visual__gt=0).count()
    pacientesAuditiva = Discapacidad.objects.filter(auditiva__gt=0).exclude(motora__gt=0).exclude(visual__gt=0).count()
    pacientesVisual = Discapacidad.objects.filter(visual__gt=0).exclude(motora__gt=0).exclude(auditiva__gt=0).count()
    pDoce=p17=pinf=0
    for p in pacientes:
        if(p.calcularEdad()<=12):
            pDoce+=1
        elif(p.calcularEdad()>12 and p.calcularEdad()<=17):
            p17+=1
        else:
            pinf+=1

        s=a=ha="False"
        if(Paciente.objects.filter(anamnesis_del_lenguaje__id=p.id).exists()):
            a="True"
        if (Paciente.objects.filter(area_social__id=p.id).exists()):
            s = "True"
        if (Paciente.objects.filter(habilidades_basicas__id=p.id).exists()):
            ha = "True";
        tmp = {"paciente": p, "edad": p.calcularEdad(), "Anamnesis": a,"Area_Social": s, "Habilidades_Basicas":ha}
        listapacientes.append(tmp)
    #listapacientes.append({"paciente": p, "edad": 19, "Anamnesis": "False","Area_Social": "False", "Habilidades_Basicas":"False"})

    return render(request,'paciente/resumen.html',
                  {"listapacientes":listapacientes,"cantidadpacientes":t,
                   'pacientesmotora':pacientesMotora,'pacientesauditiva':pacientesAuditiva,
                   'pacientesvisual':pacientesVisual,'pacientes12':pDoce,'pacientes17':p17,'pacientesinf':pinf})
def paciente_test(request):
    p=Paciente.objects.all()
    return render(request,'paciente/test.html',{"listapacientes":p})

def paciente_test_ejemplo(request):
    subprocess.Popen([settings.BASE_DIR+'p.sh'], shell=True)
    result = 'Success'
    from django.http import HttpResponse
    return HttpResponse(result)