from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
import datetime

# Create your models here.
class Paciente(models.Model):
	nombres = models.CharField(max_length=30)
	apellidos = models.CharField(max_length=30)
	fnac = models.DateField(default= 'yyyy-mm-dd')
	direccion = models.CharField(max_length=50)
	telefono = models.CharField(max_length=10)
	evaluador = models.ForeignKey('auth.User')
	fechaIngreso = models.DateField(auto_now_add=True)
	def __str__(self):
		return self.nombres+' '+self.apellidos
	def calcularEdad(self):
		hoy=datetime.date.today()
		fnac=self.fnac
		anios = ((hoy.year-fnac.year-1)+(1 if (hoy.month, hoy.day) >= (fnac.month, fnac.day) else 0))
		return anios

class Discapacidad(models.Model):
	paciente = models.ForeignKey(Paciente)
	auditiva = models.PositiveSmallIntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])
	auditiva_file = models.FileField(upload_to='./evidencia/auditiva/')
	motora = models.PositiveSmallIntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])
	motora_file = models.FileField(upload_to='./evidencia/motora/')
	visual = models.PositiveSmallIntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])
	visual_file = models.FileField(upload_to='./evidencia/visual/')
