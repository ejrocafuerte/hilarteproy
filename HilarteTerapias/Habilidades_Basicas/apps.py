from django.apps import AppConfig


class HabilidadesBasicasConfig(AppConfig):
    name = 'Habilidades Basicas'
