from django.http import HttpResponse
from reportlab.lib import colors
from io import BytesIO
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from reportlab.platypus import (Table)
from .models import Habilidades_Basicas
from Paciente.models import Paciente
from django.conf import settings
from django.contrib.auth.decorators import login_required

from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
@login_required
def reporte_habilidades(request,form_id):
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition']= 'attachment; filename="somefilename.pdf"'

	buff=BytesIO()
	try:
		data = Habilidades_Basicas.objects.get(id=form_id)
	except Habilidades_Basicas.DoesNotExist:
		data=Habilidades_Basicas()
		data.paciente=Paciente()

	back_title=colors.skyblue
	p = canvas.Canvas(buff,pagesize=A4)
	logo = settings.MEDIA_ROOT+'hilarte-logo.png'
	p.setLineWidth(.3)

	p.setFont("Helvetica",22)
	p.drawString(120,800,"Hilarte - Sistema de Consulta de Terapias")
	p.drawString(120,770,"Formulario de Habilidades Basicas")
	p.drawImage(logo, 30, 750, 80, 80,preserveAspectRatio=True)
	p.setFont("Helvetica",16)
	p.drawString(30,730,'Paciente: '+data.paciente.apellidos+" "+data.paciente.nombres)
	p.setFont("Helvetica",10)

	#Ejemplo02
	datos = (
		('Motricidad Fina','',''),
		('Mano Predominante: '+data.mano_predominante,'Corta en linea recta con tijera: '+data.manejo_de_tijera_linea_recta),
		('Hace trozado: '+data.trozado,'Hace Rasgado: '+data.rasgado,'Hace pegado: '+data.pegado),
		('Hace observaciones: '+data.observacion,'Dobla en dos partes: '+data.doblado_en_dos,'Dobla en cuatro partes: '+data.doblado_en_cuatro),
		('Motricidad Gruesa',),
		('Sentado: '+data.sentado,'Parado: '+data.parado,'Arrodillado: '+data.arrodillado),
		('Acostado: '+data.acostado,'Cruza los brazos: '+data.cruza_los_brazos,'Levanta los brazos: '+data.levanta_los_brazos),
		('Manos en la cintura: '+data.manos_en_la_cintura,'Manos en la cadera: '+data.manos_en_la_cadera,),
		('Inclina hacia delante: '+data.inclinarse_hacia_delante,'Inclina hacia atras: '+data.inclinarse_hacia_atras),
		('Pararse en puntas de pie: '+data.pararse_puntas_de_pies,'Avanza: '+data.avanza,'Retrocede: '+data.retrocede),
		('Sensopercepcion',''),
		('Cual es el Circulo: '+data.cual_es_circulo,'Cual es el Cuadrado: '+data.cual_es_cuadrado,'Cual es el Triangulo: '+data.cual_es_triangulo),
		('Cual es el rectangulo: '+data.cual_es_rectangulo,),
		('Dame el Rojo: '+data.dame_el_rojo,'Dame el amarillo: '+data.dame_el_amarillo,'Dame el azul:'+data.dame_el_azul),
		('Dame el verde: '+data.dame_el_verde,'Dame el anaranjado: '+data.dame_el_anaranjado),
		('Esquema Corporal',),
		('Ubica tu cabeza: '+data.donde_esta_tu_cabeza,'Ubica tu cara: '+data.donde_esta_tu_cara,'Ubica tu pelo: '+data.donde_esta_tu_pelo),
		('Ubica tus orejas: '+data.donde_esta_tus_orejas,'Ubica tus ojos: '+data.donde_esta_tus_ojos,'Ubica tu lengua: '+data.donde_esta_tu_lengua),
		('Ubica tus dientes: '+data.donde_esta_tus_dientes,'Ubica tu boca: '+data.donde_esta_tu_boca,'Ubica tu tronco: '+data.donde_esta_tu_tronco),
		('Ubica tus hombros: '+data.donde_esta_tu_hombro,'Ubica tus manos: '+data.donde_esta_tus_manos,'Ubica tus dedos: '+data.donde_esta_tus_dedos),
		('Ubica tu cintura: '+data.donde_esta_tu_cintura,'Ubica tus nalgas: '+data.donde_esta_tu_nalga,'Ubica tu pene: '+data.donde_esta_tu_pene),
		('Ubica tu pie: '+data.donde_esta_tu_pie,'Ubica tu rodilla: '+data.donde_esta_tu_rodilla,'Ubica tu hombligo: '+data.donde_esta_tu_ombligo),
		('Orientacion Espacial',''),
		('Muñeco delante: '+data.pon_muñeco_delante,'Muñeco detras: '+data.pon_muñeco_detras,'Muñeco en el medio: '+data.pon_muñeco_en_el_medio),
		('Muñeco al lado: '+data.pon_muñeco_al_lado,'Muñeco arriba: '+data.pon_muñeco_arriba,'Muñeco abajo: '+data.pon_muñeco_abajo),
		('Muñeco afuera: '+data.pon_muñeco_afuera,'Muñeco adentro: '+data.pon_muñeco_adentro,'Muñeco en la esquina: '+data.pon_muñeco_en_la_esquina),
		('Dia: '+data.dia,'Noche: '+data.noche,'Lejos: '+data.lejos),
		('Cerca: '+data.cerca,'Rapido: '+data.rapido,'Lento: '+data.lento),
		('Derecha: '+data.derecha,'Izquierda: '+data.izquierda,),
	)
	tabla = Table(data = datos,vAlign='TOP',
				  style = [
						   ('GRID',(0,0),(-1,-1),0.5,colors.grey),
						   ('BOX',(0,0),(-1,-1),2,colors.black),
						   ('BACKGROUND', (0, 0), (-1, 0), back_title),
					  		('BACKGROUND', (0, 4), (-1, 4), back_title),
					  ('BACKGROUND', (0, 10), (-1, 10), back_title),
					  ('BACKGROUND', (0, 15), (-1, 15), back_title),
					  ('BACKGROUND', (0, 22), (-1, 22), back_title),
					  		]
				  )
	tabla.wrapOn(p,210,540)
	tabla.drawOn(p,30,200)
	p.showPage()
	p.save()
	pdf = buff.getvalue()

	buff.close()
	response.write(pdf)
	return response