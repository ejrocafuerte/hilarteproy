from django.db import models
from Paciente.models import Paciente
# Create your models here.

class Habilidades_Basicas(models.Model):
#class Motricidad_Fina(models.Model):
	class Meta:
		verbose_name_plural = "Habilidades Basicas"

	paciente = models.ForeignKey(Paciente,on_delete=models.CASCADE)
	fecha_formulario = models.DateTimeField(auto_now_add=True)
	archivo_habilidades = models.FileField(upload_to='pruebas/habilidades/')
	Derecha='D'
	Izquierda='I'
	Ambas='A'
	mano_op=(
		(Derecha,'Derecha'),
		(Izquierda,'Izquierda'),
		(Ambas,'Ambas')
	)
	SI='SI'
	NO='NO'
	bool_op=(
		(SI,'SI'),
		(NO,'NO')
	)
	mano_predominante=models.CharField(max_length=1,choices=mano_op)
	manejo_de_tijera_linea_recta=models.CharField(max_length=2,choices=bool_op)
	trozado=models.CharField(max_length=2,choices=bool_op)
	rasgado=models.CharField(max_length=2,choices=bool_op)
	pegado=models.CharField(max_length=2,choices=bool_op)
	observacion=models.TextField()
	doblado_en_dos=models.CharField(max_length=2,choices=bool_op)
	doblado_en_cuatro=models.CharField(max_length=2,choices=bool_op)

#class Motricidad_Gruesa(models.Model):
	sentado=models.CharField(max_length=2,choices=bool_op)
	parado=models.CharField(max_length=2,choices=bool_op)
	arrodillado=models.CharField(max_length=2,choices=bool_op)
	acostado=models.CharField(max_length=2,choices=bool_op)
	cruza_los_brazos=models.CharField(max_length=2,choices=bool_op)
	levanta_los_brazos=models.CharField(max_length=2,choices=bool_op)
	manos_en_la_cintura=models.CharField(max_length=2,choices=bool_op)
	manos_en_la_cadera=models.CharField(max_length=2,choices=bool_op)
	inclinarse_hacia_delante=models.CharField(max_length=2,choices=bool_op)
	inclinarse_hacia_atras=models.CharField(max_length=2,choices=bool_op)
	pararse_puntas_de_pies=models.CharField(max_length=2,choices=bool_op)
	avanza=models.CharField(max_length=2,choices=bool_op)
	retrocede=models.CharField(max_length=2,choices=bool_op)

#class Sensopercepcion(models.Model):
	cual_es_circulo=models.CharField(max_length=2,choices=bool_op)
	cual_es_cuadrado=models.CharField(max_length=2,choices=bool_op)
	cual_es_triangulo=models.CharField(max_length=2,choices=bool_op)
	cual_es_rectangulo=models.CharField(max_length=2,choices=bool_op)

	dame_el_rojo=models.CharField(max_length=2,choices=bool_op)
	dame_el_amarillo=models.CharField(max_length=2,choices=bool_op)
	dame_el_azul=models.CharField(max_length=2,choices=bool_op)
	dame_el_verde=models.CharField(max_length=2,choices=bool_op)
	dame_el_anaranjado=models.CharField(max_length=2,choices=bool_op)

#class Esquema_Corporal(models.Model):
	donde_esta_tu_cabeza=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_cara=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_pelo=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tus_orejas=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tus_ojos=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_lengua=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tus_dientes=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_boca=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_tronco=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_hombro=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tus_manos=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tus_dedos=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_cintura=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_nalga=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_pene=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_pie=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_rodilla=models.CharField(max_length=2,choices=bool_op)
	donde_esta_tu_ombligo=models.CharField(max_length=2,choices=bool_op)

#class Orientacion_Espacial(models.Model):

	pon_muñeco_delante=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_detras=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_en_el_medio=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_al_lado=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_arriba=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_abajo=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_afuera=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_adentro=models.CharField(max_length=2,choices=bool_op)
	pon_muñeco_en_la_esquina=models.CharField(max_length=2,choices=bool_op)
	
	dia=models.CharField(max_length=2,choices=bool_op)
	noche=models.CharField(max_length=2,choices=bool_op)
	lejos=models.CharField(max_length=2,choices=bool_op)
	cerca=models.CharField(max_length=2,choices=bool_op)
	rapido=models.CharField(max_length=2,choices=bool_op)
	lento=models.CharField(max_length=2,choices=bool_op)
	
	derecha=models.CharField(max_length=2,choices=bool_op)
	izquierda=models.CharField(max_length=2,choices=bool_op)