"""HilarteTerapias URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from Anamnesis_del_Lenguaje import views as anamnesis_views
from Area_Social import views as social_views
from Habilidades_Basicas import views as habilidades_views
from Paciente import views as paciente_views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/anamnesis/([0-9]+)',anamnesis_views.reporte_anamnesis),
    url(r'^admin/social/([0-9]+)',social_views.reporte_social),
    url(r'^admin/habilidades/([0-9]+)',habilidades_views.reporte_habilidades,name='reporte_habilidades'),
    url(r'^admin/estado/',paciente_views.resumen_sistema,name='reporte_sistema'),
    url(r'^admin/estado/',paciente_views.resumen_sistema,name='reporte_sistema'),
    url(r'^admin/paciente/test',paciente_views.paciente_test,name='test_usuarios'),
    url(r'^admin/paciente/test/ejemplo',paciente_views.paciente_test_ejemplo,name='test_usuarios_ejemplo'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


