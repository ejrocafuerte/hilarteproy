from django.db import models
from Paciente.models import Paciente
# Create your models here.

#class Que_Cosas_Buenas_Hace_Hijo(models.Model):
class Area_Social(models.Model):
	class Meta:
		verbose_name_plural = "Area Social"
	
	paciente = models.ForeignKey(Paciente,on_delete=models.CASCADE)
	fecha_formulario = models.DateTimeField(auto_now_add=True)
	archivo_social = models.FileField(upload_to='pruebas/social/')
	SI='SI'
	NO='NO'
	bool_op=(
		(SI,'SI'),
		(NO,'NO')
	)
	suena_nariz=models.CharField(max_length=2, choices=bool_op)
	utiles_aseo=models.CharField(max_length=2, choices=bool_op)
	baja_sube_interior=models.CharField(max_length=2, choices=bool_op)
	zapatos=models.CharField(max_length=2, choices=bool_op)
	come_solo=models.CharField(max_length=2, choices=bool_op)
	avisa_baño=models.CharField(max_length=2, choices=bool_op)
	lava_seca_manos=models.CharField(max_length=2, choices=bool_op)
	independencia_padres=models.CharField(max_length=2, choices=bool_op)
	duerme_solo=models.CharField(max_length=2, choices=bool_op)

	respeta_turno=models.CharField(max_length=2, choices=bool_op)
	comunica_necesidades=models.CharField(max_length=2, choices=bool_op)
	juega_otros_simples=models.CharField(max_length=2, choices=bool_op)
	juega_otros_complejas=models.CharField(max_length=2, choices=bool_op)
	responde_caricias=models.CharField(max_length=2, choices=bool_op)
	expresa_sentimientos=models.CharField(max_length=2, choices=bool_op)
	se_defiende=models.CharField(max_length=2, choices=bool_op)
	puesto_hora_trabajo=models.CharField(max_length=2, choices=bool_op)
	comporta_bien_publico=models.CharField(max_length=2, choices=bool_op)
	sigue_instrucciones=models.CharField(max_length=2, choices=bool_op)
	comparte_pertenencias=models.CharField(max_length=2, choices=bool_op)

	diferencia_nino_a=models.CharField(max_length=2, choices=bool_op)
	identifica_partes_cuerpo=models.CharField(max_length=2, choices=bool_op)
	reconoce_organos_reproductores=models.CharField(max_length=2, choices=bool_op)
	cierra_perta_bano=models.CharField(max_length=2, choices=bool_op)
	busca_privacidad=models.CharField(max_length=2, choices=bool_op)
	reconoce_prendas_nino_a=models.CharField(max_length=2, choices=bool_op)
	preparado_rechazar_caricias_extr=models.CharField(max_length=2, choices=bool_op)
	lugar_apropiado_necesidades=models.CharField(max_length=2, choices=bool_op)

#class Que_Preocupa_de_mi_Hijo(models.Model):
	muerde=models.CharField(max_length=2,choices=bool_op)
	escupe=models.CharField(max_length=2,choices=bool_op)
	empuja_rasguna_pellizca=models.CharField(max_length=2,choices=bool_op)
	golpea=models.CharField(max_length=2,choices=bool_op)
	hala_oreja_cabello=models.CharField(max_length=2,choices=bool_op)
	moja_baño=models.CharField(max_length=2,choices=bool_op)

	llora_grita=models.CharField(max_length=2,choices=bool_op)
	tira_piso_grita_chilla=models.CharField(max_length=2,choices=bool_op)
	patea_mientras_golpea_obj=models.CharField(max_length=2,choices=bool_op)
	tira_golpea_puertas=models.CharField(max_length=2,choices=bool_op)

#class Responde_a_Necesidades_del_Hijo(models.Model):
	niño_visita_al_medico=models.CharField(max_length=2,choices=bool_op)
	buena_nutricion=models.CharField(max_length=2,choices=bool_op)
	cuida_aseo_del_cuerpo=models.CharField(max_length=2,choices=bool_op)
	cuida_vestimenta_acorde_edad=models.CharField(max_length=2,choices=bool_op)
	
	comunicamos_amor=models.CharField(max_length=2,choices=bool_op)
	alabamos_por_intentar=models.CharField(max_length=2,choices=bool_op)
	preparados_para_aceptar_problema=models.CharField(max_length=2,choices=bool_op)
	
	participamos_excursion_actividades=models.CharField(max_length=2,choices=bool_op)
	averigua_ayuda_adicional=models.CharField(max_length=2,choices=bool_op)
	escuhan_musica_agrado_niño=models.CharField(max_length=2,choices=bool_op)
	proveen_juguetes_materiales_pedagogicos=models.CharField(max_length=2,choices=bool_op)

	cercioramos_que_siga_reglas=models.CharField(max_length=2,choices=bool_op)
	enfoque_positivo_enseñanza=models.CharField(max_length=2,choices=bool_op)
	decimos_lo_que_tiene_que_hacer=models.CharField(max_length=2,choices=bool_op)

	buen_modelo_para_hijos=models.CharField(max_length=2,choices=bool_op)
	respetamos_niño=models.CharField(max_length=2,choices=bool_op)
	escuchamos_atencion=models.CharField(max_length=2,choices=bool_op)
	enseñamos_habilidades=models.CharField(max_length=2,choices=bool_op)
	ambiente_fisico_es_organizado_limpio=models.CharField(max_length=2,choices=bool_op)

	fomentamos_independencia_del_niño=models.CharField(max_length=2,choices=bool_op)

	ayudamos_a_hablar_de_sus_sentimientos=models.CharField(max_length=2,choices=bool_op)
	
