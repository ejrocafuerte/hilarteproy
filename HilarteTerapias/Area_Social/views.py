from django.http import HttpResponse
from reportlab.lib import colors
from io import BytesIO
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from reportlab.platypus import (Table)
from .models import Area_Social
from Paciente.models import Paciente
from django.conf import settings
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def reporte_social(request,form_id):
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition']= 'attachment; filename="somefilename.pdf"'

	buff=BytesIO()
	try:
		data = Area_Social.objects.get(id=form_id)
	except Area_Social.DoesNotExist:
		data=Area_Social()
		data.paciente=Paciente()


	back_title=colors.skyblue
	p = canvas.Canvas(buff,pagesize=A4)
	logo = settings.MEDIA_ROOT+'hilarte-logo.png'
	p.setLineWidth(.3)

	p.setFont("Helvetica",22)
	p.drawString(120,800,"Hilarte - Sistema de Consulta de Terapias")
	p.drawString(120,770,"Formulario de Area Social")
	p.drawImage(logo, 30, 750, 80, 80,preserveAspectRatio=True)
	p.setFont("Helvetica",16)
	p.drawString(30,730,'Paciente: '+data.paciente.apellidos+" "+data.paciente.nombres)
	p.setFont("Helvetica",10)

	#Ejemplo02
	datos = (
		('Cosas Buenas que hace mi Hijo',''),
		('Suena la nariz: '+data.suena_nariz,'Usa utiles de Aseo: '+data.utiles_aseo),
		('Se baja y sube el interior: '+data.baja_sube_interior,'Pone los zapatos: '+data.zapatos),
		('Come solo: '+data.come_solo,'Avisa ir al baño: '+data.avisa_baño),
		('Lava y seca sus manos: '+data.lava_seca_manos, 'Independiente de padres: '+data.independencia_padres),
		('Duerme solo: '+data.duerme_solo,),
		('Respeta el turno de los demas:'+data.respeta_turno,''),
		('Comunica sus necesidades: '+data.comunica_necesidades,'Juega con otros juegos simples: '+data.juega_otros_simples),
		('Juega con otros juegos complejos: '+data.juega_otros_complejas,'Responde a caricias: '+data.responde_caricias),
		('Espresa sus sentimientos: '+data.expresa_sentimientos, 'Se defiende: '+data.se_defiende),
		('Deja cosas en su puesto a la hora de trabajar: '+data.puesto_hora_trabajo,'Se comporta bien en publico: '+data.comporta_bien_publico),
		('Sigue instrucciones: '+data.sigue_instrucciones, 'Comparte sus pertenencias con otros: '+data.comparte_pertenencias),
		('Diferencia niños y niñas: '+data.diferencia_nino_a,'Identifica partes del cuerpo: '+data.identifica_partes_cuerpo),
		('Conoce organos reproductores: '+data.reconoce_organos_reproductores,'Cierra la puerta del baño: '+data.cierra_perta_bano),
		('Busca su privacidad: '+data.busca_privacidad,'Reconoce diferencias en prendas de niño y niña: '+data.reconoce_prendas_nino_a),
		('Preparado para rechazar caricias de extraños: '+data.preparado_rechazar_caricias_extr,"Hace sus necesidades en el lugar apropiado: "+data.lugar_apropiado_necesidades),
		('Cosas que me preocupan de mi hijo',''),
		('Muerde: '+data.muerde,'Escupe: '+data.escupe),
		('Empuja - Rasguña - Pellizca: '+data.empuja_rasguna_pellizca,'Golpea: '+data.golpea),
		('Hala oreja o cabello: '+data.hala_oreja_cabello,'Moja el baño: '+data.moja_baño),
		('Llora - Grita: '+data.llora_grita,'Se tira al piso o chilla: '+data.tira_piso_grita_chilla),
		('Patalea mientras golpea objetos: '+data.patea_mientras_golpea_obj,'Tira o golpea puertas: '+data.tira_golpea_puertas),
		('Respondo a las necesidades de mi Hijo',''),
		('Va al medico: '+data.niño_visita_al_medico,'Tiene buena nutricion: '+data.buena_nutricion),
		('Cuido el aseo de su cuerpo: '+data.cuida_aseo_del_cuerpo,'Cuido su vestimenta acorde la edad: '+data.cuida_vestimenta_acorde_edad),
		('Le comunicamos amor: '+data.comunicamos_amor,'Lo alabamos por intentar: '+data.alabamos_por_intentar),
		('Estamos preparados para aceptar problemas: '+data.preparados_para_aceptar_problema ,'Participamos en sus actividades: '+data.participamos_excursion_actividades),
		('Averiguamos ayuda adicional: '+data.averigua_ayuda_adicional,'Escuchan musica del agrado del niño:'+data.escuhan_musica_agrado_niño),
		('Proveen de juguetes y materiales pedagogicos: '+data.proveen_juguetes_materiales_pedagogicos,'Nos cercioramos de que siga reglas: '+data.cercioramos_que_siga_reglas),
		('Damos enfoque positivo a la enseñanza: '+data.enfoque_positivo_enseñanza,'Le decimos lo que tiene que hacer: '+data.decimos_lo_que_tiene_que_hacer),
		('Somos un buen modelo para el niño: '+data.buen_modelo_para_hijos,'Respetamos a nuestro hijo: '+data.respetamos_niño),
		('Escuchamos con atencion: '+data.escuchamos_atencion,'Enseñamos habilidades: '+data.enseñamos_habilidades),
		('Su ambiente es organizado y limpio: '+data.ambiente_fisico_es_organizado_limpio,'Fomentamos la independencia del niño: '+data.fomentamos_independencia_del_niño),
		('Le ayudamos ahablar de sus sentimientos: '+data.ayudamos_a_hablar_de_sus_sentimientos,''),
	)
	tabla = Table(data = datos,vAlign='TOP',
				  style = [
						   ('GRID',(0,0),(-1,-1),0.5,colors.grey),
						   ('BOX',(0,0),(-1,-1),2,colors.black),
						   ('BACKGROUND', (0, 0), (-1, 0), back_title),
					  		('BACKGROUND', (0, 16), (-1, 16), back_title),
					  		('BACKGROUND', (0, 22), (-1, 22), back_title),
						   ]
				  )
	tabla.wrapOn(p,210,540)
	tabla.drawOn(p,30,100)
	p.showPage()
	p.save()
	pdf = buff.getvalue()

	buff.close()
	response.write(pdf)
	return response
