# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-08 21:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Paciente', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Area_Social',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_formulario', models.DateTimeField(auto_now_add=True)),
                ('archivo_social', models.FileField(upload_to='pruebas/<django.db.models.fields.related.ForeignKey>/')),
                ('suena_nariz', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('utiles_aseo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('baja_sube_interior', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('zapatos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('come_solo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('avisa_baño', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lava_seca_manos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('independencia_padres', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('duerme_solo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('respeta_turno', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comunica_necesidades', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('juega_otros_simples', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('juega_otros_complejas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('responde_caricias', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('expresa_sentimientos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('se_defiende', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('puesto_hora_trabajo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comporta_bien_publico', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('sigue_instrucciones', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comparte_pertenencias', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('diferencia_nino_a', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('identifica_partes_cuerpo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('reconoce_organos_reproductores', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cierra_perta_bano', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('busca_privacidad', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('reconoce_prendas_nino_a', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('preparado_rechazar_caricias_extr', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lugar_apropiado_necesidades', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('muerde', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('escupe', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('empuja_rasguna_pellizca', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('golpea', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('hala_oreja_cabello', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('moja_baño', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('llora_grita', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('tira_piso_grita_chilla', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('patea_mientras_golpea_obj', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('tira_golpea_puertas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('niño_visita_al_medico', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('buena_nutricion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cuida_aseo_del_cuerpo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cuida_vestimenta_acorde_edad', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comunicamos_amor', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('alabamos_por_intentar', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('preparados_para_aceptar_problema', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('participamos_excursion_actividades', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('averigua_ayuda_adicional', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('escuhan_musica_agrado_niño', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('proveen_juguetes_materiales_pedagogicos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cercioramos_que_siga_reglas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('enfoque_positivo_enseñanza', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('decimos_lo_que_tiene_que_hacer', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('buen_modelo_para_hijos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('respetamos_niño', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('escuchamos_atencion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('enseñamos_habilidades', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('ambiente_fisico_es_organizado_limpio', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('fomentamos_independencia_del_niño', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('ayudamos_a_hablar_de_sus_sentimientos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('paciente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Paciente.Paciente')),
            ],
            options={
                'verbose_name_plural': 'Area Social',
            },
        ),
    ]
