import os
#Librerias reportlab a usar:
from reportlab.platypus import (Table)
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from django.http import HttpResponse
from io import BytesIO
from reportlab.pdfgen import canvas
from .models import Anamnesis_del_Lenguaje
from Paciente.models import Paciente
from django.conf import settings
from django.contrib.auth.decorators import login_required

@login_required
def reporte_anamnesis(request,form_id):
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition']= 'attachment; filename="somefilename.pdf"'

	buff=BytesIO()
	try:
		data = Anamnesis_del_Lenguaje.objects.get(id=form_id)
	except Anamnesis_del_Lenguaje.DoesNotExist:
		data=Anamnesis_del_Lenguaje()
		data.paciente=Paciente()

	back_title=colors.skyblue
	p = canvas.Canvas(buff,pagesize=A4)
	logo = settings.MEDIA_ROOT+'hilarte-logo.png'
	p.setLineWidth(.3)

	p.setFont("Helvetica",22)
	p.drawString(120,800,"Hilarte - Sistema de Consulta de Terapias")
	p.drawString(120,770,"Formulario de Anamnesis del Lenguaje")
	p.drawImage(logo, 30, 750, 80, 80,preserveAspectRatio=True)
	p.setFont("Helvetica",16)
	p.drawString(30,730,'Paciente: '+data.paciente.apellidos+" "+data.paciente.nombres)
	p.setFont("Helvetica",10)

	#Ejemplo02
	datos = (
	        ('Datos del Padre','',''),
	        ('Nombres: '+data.nombre_del_padre, 'Edad: '+data.edad_del_padre,'Ocupacion: '+data.ocupacion_del_padre,),
	        ( 'Antecedentes del Padre: '+data.antecedentes_del_padre,''),
            ('Datos de la Madre','',''),
	        ('Nombres: '+data.nombre_de_la_madre, 'Edad: '+data.edad_de_la_madre,'Ocupacion: '+data.ocupacion_de_la_madre,),
	        ('Antecedentes de la Madre: '+data.antecedentes_de_la_madre,'Hijos de la madre: '+str(data.numero_de_hijos_de_la_madre)),
            ('Familiares con problemas de Lenguaje','',''),
            ('Nombre del Familiar: '+data.nombre_del_familiar,'Parentesco: '+data.parentesco),
            ('Antecedentes Generales','',''),
            ('Duracion del Embarazo (meses): '+str(data.duracion_del_embarazo),'Tipo de parto: '+data.tipo_de_parto),
            ('Enfermedades durante embarazo: '+data.enfermedades_durante_embarazo,'Problemas de Glucosa: '+data.problemas_glucosa, 'Edemas: '+data.edemas),
            ('Fiebre Alta: '+data.fiebre_alta,'Radiografias: '+data.radiografias,'Hemorragias: '+data.hemorragias),
            ('Alimentacion: '+data.alimentacion,'Lugar de Nacimiento: '+data.lugar_nacimiento,'Peso: '+str(data.peso)),
            ('Talla: '+str(data.talla),'Anestesia: '+data.anestesia,'Vacunas Completas: '+data.vacunas_completas),
            ('Desarrollo General del Niño',''),
            ('Alimentacion: '+data.alimentacion,'Dificultad para Tragar: '+data.dificultad_tragar,'Denticion: '+data.denticion),
            ('Motricidad',''),
            ('Cuello Firme: '+data.cuello_firme, 'Sedentacion: '+data.sedentacion,'Bipedestacion: '+data.bipedestacion),
            ('Lateralidad Mano que come: '+data.lateralidad_mano_come,'Lateralidad Mano que escribe: '+data.lateralidad_mano_escribe,'Marcha Final: '+data.marcha_final),
            ('Lateralidad Mano que dibuja: '+data.lateralidad_mano_dibuja,''),
            ('Primeros Sonidos',''),
            ('Primeros Sonidos: '+data.primeros_sonidos,'Balbuceo: '+data.balbuceo,'Gestos para Pedir: '+data.gestos_pedir),
            ('Primeras Palabras: '+data.primeras_palabras,'Entiende Palabras Comunes: '+data.entiende_comunes,'Otras Palabras: '+data.palabras_aparte),
            ('Entiende Combinaciones: '+data.combinacion_palabras,'Oraciones completas: '+data.oraciones_completas,'Idioma en casa: '+data.idioma_casa),
            ('Con quien habla mas: '+data.mas_comunica,'Habla a juguestes: '+data.habla_juguetes,'Imita a: '+data.imita),
            ('Factores del Problema: '+data.factores_problema,'Ayudas a la solucion: '+data.ayudas_problema),
            ('Audicion',''),
            ('Reacciones',''),
            ('Sonidos bajos: '+data.reaccion_sonidos_bajos,'Sonidos medios: '+data.reaccion_sonidos_medios,'Sonidos altos: '+data.reaccion_sonidos_altos),
            ('Voz alta: '+data.reaccion_voz_alta,'Voz baja: '+data.reaccion_voz_baja,'Voz susurro: '+data.reaccion_voz_susurro),
            ('Voz normal: '+data.reaccion_voz_normal,'Voz anormal: '+data.reaccion_voz_anormal),
            ('Psicologica Emocional',''),
            ('Control de Esfinteres: '+data.control_esfinteres,'Sobreproteccion: '+data.sobreproteccion,'Introversion: '+data.introversion),
            ('Agresividad: '+data.agresividad,'Colaboracion: '+data.colaboracion,'Dependencia: '+data.dependencia),
            ('Extroversion: '+data.extroversion,'Egoismo: '+data.egoismo,'Desobediencia: '+data.desobediencia),
            ('Escolaridad',''),
            ('Jardin: '+data.jardin,'Primaria: '+data.primaria,'Aprovechamiento: '+str(data.aprovechamiento)),
            ('Relaciones con Profesores: '+data.relaciones_profesores,'Años repetidos: '+str(data.años_repetidos),'Motivos: '+data.porque_repetidos),

	    )
	tabla = Table(data = datos,vAlign='TOP',
	              style = [
	                       ('GRID',(0,0),(-1,-1),0.5,colors.grey),
	                       ('BOX',(0,0),(-1,-1),2,colors.black),
	                       ('BACKGROUND', (0, 0), (-1, 0), back_title),
                           ('BACKGROUND', (0, 3), (-1, 3), back_title),
                           ('BACKGROUND', (0, 6), (-1, 6), back_title),
                            ('BACKGROUND', (0, 8), (-1, 8), back_title),
                            ('BACKGROUND', (0, 14), (-1, 14),back_title),
                            ('BACKGROUND', (0, 16), (-1, 16), back_title),
                            ('BACKGROUND', (0, 20), (-1, 20), back_title),
                            ('BACKGROUND', (0, 26), (-1, 27), back_title),
                            ('BACKGROUND', (0, 31), (-1, 31), back_title),
                            ('BACKGROUND', (0, 35), (-1, 35), back_title),
	                       ]
	              )
	tabla.wrapOn(p,210,540)
	tabla.drawOn(p,30,40)
	p.showPage()
	

	p.setLineWidth(.3)

	p.setFont("Helvetica",22)
	p.drawString(120,800,"Hilarte - Sistema de Consulta de Terapias")
	p.drawString(120,770,"Formulario de Anamnesis del Lenguaje")
	p.drawImage(logo, 30, 750, 80, 80,preserveAspectRatio=True)
	p.setFont("Helvetica",16)
	p.drawString(30,730,'Paciente: '+data.paciente.apellidos+" "+data.paciente.nombres)
	p.setFont("Helvetica",10)
	datos2=(
		('Respiracion','',''),
		('Tipo de Respiracion: '+data.tipo_de_respiracion,'Simetria Toraxica: '+data.simetria_toraxica),
		('Respiracion: '+data.respiracion,'Expiracion: '+data.expiracion),
        ('Antecedentes Patologicos',''),
        ('Enfermedades: '+data.enfermedades,'Fiebres: '+data.fiebres,'Convulsiones: '+data.convulsiones),
        ('Traumatismos: '+data.traumatismos,'Operaciones: '+data.operaciones,'Afecciones del oido: '+data.afecciones_oido),
        ('Otros: '+data.otros,'Examenes y Tratamientos: '+data.examenes_tratamientos),
        ('BucoFaringeo',''),
        ('Forma de los labios: '+data.forma_labios,'Posicion de los Labios: '+data.posicion_labios,'Tamaño de la lengua: '+data.tamaño_lengua),
        ('Frenillo sublingual superior: '+data.frenillo_sublingual_superior,'Frenillo sublingual inferior: '+data.frenillo_sublingual_inferior),
        ('Movimientos Linguales',''),
        ('Superior: '+data.movimiento_lingual_arriba,'Inferior: '+data.movimiento_lingual_abajo,'Izq - Der: '+data.movimiento_lingual_id),
        ('Izquierda: '+data.movimiento_lingual_i,'Derecha: '+data.movimiento_lingual_d,'Interna: '+data.vibracion_lingual_interna),
        ('Lenguaje Expresivo',''),
        ('Emite sonidos finales: '+data.emite_sonidos_finales,'Emite sonidos silabas: '+data.emite_sonidos_silabas,'Emite sonidos palabras: '+data.emite_sonidos_palabras),
        ('Cadencia Lenta: '+data.cadencia_lenta,'Cadencia Normal: '+data.cadencia_normal,'Cadencia precipitada: '+data.cadencia_precipitada),
        ('Ritmo de Tartamudeo: '+data.ritmo_tartamudeo,'Ritmo regular: '+data.ritmo_regular,'Ritmo golpe: '+data.ritmo_golpe),
        ('Pausas Interrupciones: '+data.pausas_interrupciones,'Pensamientos mal formulados: '+data.pensamiento_mal_formulado,'Carece de ideas: '+data.carece_ideas),
        ('Lenguaje Comprensivo',''),
        ('Comprende lo que copia: '+data.comprende_copia,'Comprende nombres: '+data.comprende_nombres),
        ('Sopa: '+data.asocia_ideas_sopa,'Lavadora: '+data.asocia_ideas_lava,'Agua: '+data.asocia_ideas_agua),
        ('Viaje: '+data.asocia_ideas_viaje,'Escuela - pizzarra: '+data.asocia_ideas_esc_piz),
        ('Lectura',''),
        ('Forma que lee: '+data.forma_lee,'Palabras de silabas: '+data.lee_palabras_silabas, 'Silabas trabadas: '+data.silabas_trabadas),
        ('Errores de sustitucion: '+data.lee_errores_sustitucion,'Errores Omision: '+data.lee_errores_omision,'Errores Rotacion: '+data.lee_errores_rotacion),
        ('Errores adicion: '+data.lee_errores_adicion,'Comprende lo que lee: '+data.comprende_lee),
        ('Comprension de Numeros',''),
        ('Corto: '+data.corto,'Largo: '+data.largo, 'Poco: '+data.poco),
        ('Mucho: '+data.mucho,'Grande: '+data.grande,'Pequeño: '+data.pequeño),
        ('Lleno: '+data.lleno, 'Vacio: '+data.vacio),
        ('Numero 0: '+data.n0,'Numero 2: '+data.n2,'Numero 3: '+data.n3),
        ('Numero 4: '+data.n4,'Numero 5: '+data.n5,'Numero 6: '+data.n6),
        ('Numero 7: '+data.n7,'Numero 8: '+data.n8,'Numero 9: '+data.n9),
        ('Memoria Auditiva',''),
        ('Mesa - Pan: '+data.mesa_pan,'Casa - Taza: '+data.casa_taza,'Col - Cal - Sal: '+data.col_cal_sal),
        ('Lapiz - Mesa - Silla: '+data.lapiz_mesa_silla_carro,'Dedo - Mano - Pie: '+data.dedo_mano_pie_codo,'Verde - Verano - Prado: '+data.verde_verano_prado_botella),
        ('Pistola - oreja - pañuelo: '+data.pistola_oreja_pañuelo,'Auto - Vaso - Libro: '+data.auto_vaso_libro_pluma,'Animales: '+data.animales),
        ('Frutas: '+data.frutas, 'Prendas de Vestir: '+data.prendas_vestir)

		)
	tabla2 = Table(data = datos2,vAlign='TOP',
		style = [
		('GRID',(0,0),(-1,-1),0.5,colors.grey),
		('BOX',(0,0),(-1,-1),2,colors.black),
		('BACKGROUND', (0, 0), (-1, 0), back_title),
        ('BACKGROUND', (0, 3), (-1, 3), back_title),
        ('BACKGROUND', (0, 7), (-1, 7), back_title),
        ('BACKGROUND', (0, 10), (-1, 10), back_title),
        ('BACKGROUND', (0, 13), (-1, 13), back_title),
        ('BACKGROUND', (0, 18), (-1, 18), back_title),
        ('BACKGROUND', (0, 22), (-1, 22), back_title),
        ('BACKGROUND', (0, 26), (-1, 26), back_title),
        ('BACKGROUND', (0, 33), (-1, 33), back_title),
		]
		)
	tabla2.wrapOn(p,210,540)
	tabla2.drawOn(p,30,40)
	p.showPage()
	p.save()
	pdf = buff.getvalue()
    
	buff.close()
	response.write(pdf)
	return response
