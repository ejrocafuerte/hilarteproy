from django.contrib import admin
from django.http import HttpResponseRedirect
#desde app Anamnesis del Lenguaje
from .models import Anamnesis_del_Lenguaje
from Area_Social.models import Area_Social
from Habilidades_Basicas.models import Habilidades_Basicas
from Paciente.models import Paciente
from Paciente.models import Discapacidad
from django.shortcuts import redirect

# Register your models here.
def descarga_formulario(AnamnesisAdmin, request, queryset):
	descarga_formulario.short_description = "Marcar Formularios a descargar"
	url= '/admin/anamnesis/' +str(queryset[0].pk)
	return redirect(url)


class AnamnesisAdmin(admin.ModelAdmin):
	list_display=(
		'paciente','nombre_del_padre','edad_del_padre','ocupacion_del_padre','antecedentes_del_padre','nombre_de_la_madre',
						'edad_de_la_madre','ocupacion_de_la_madre','antecedentes_de_la_madre','numero_de_hijos_de_la_madre',
					'nombre_del_familiar','parentesco',
					'duracion_del_embarazo','tipo_de_parto','enfermedades_durante_embarazo','problemas_glucosa','edemas',
						'fiebre_alta','radiografias','hemorragias','alimentacion','lugar_nacimiento','peso','talla','anestesia',
						'vacunas_completas',
					'alimentacion','dificultad_tragar','denticion',
					'cuello_firme','sedentacion','bipedestacion','marcha_final','lateralidad_mano_come','lateralidad_mano_escribe',
						'lateralidad_mano_dibuja',
					'primeros_sonidos','balbuceo','gestos_pedir','primeras_palabras','palabras_aparte','entiende_comunes',
						'combinacion_palabras','oraciones_completas','idioma_casa','imita','habla_juguetes','mas_comunica',
						'factores_problema','ayudas_problema',
					'reaccion_sonidos_bajos','reaccion_sonidos_medios','reaccion_sonidos_altos','reaccion_voz_alta',
						'reaccion_voz_baja','reaccion_voz_susurro','reaccion_voz_normal','reaccion_voz_anormal',
					'control_esfinteres','sobreproteccion','introversion','agresividad','colaboracion','dependencia','extroversion',
						'egoismo','desobediencia',
					'jardin','primaria','años_repetidos','porque_repetidos','aprovechamiento','relaciones_profesores',
					'tipo_de_respiracion','simetria_toraxica','respiracion','expiracion',
					'enfermedades','fiebres','convulsiones','traumatismos','operaciones','afecciones_oido','otros',
						'examenes_tratamientos',
					'forma_labios','frenillo_sublingual_superior','frenillo_sublingual_inferior','posicion_labios','tamaño_lengua',
						'movimiento_lingual_arriba','movimiento_lingual_abajo','movimiento_lingual_id','movimiento_lingual_i',
						'movimiento_lingual_d','vibracion_lingual_interna',
					
					'emite_sonidos_finales','emite_sonidos_silabas','emite_sonidos_palabras','cadencia_lenta','cadencia_normal',
						'cadencia_precipitada','ritmo_tartamudeo','ritmo_regular','ritmo_golpe','pausas_interrupciones',
						'pensamiento_mal_formulado','carece_ideas',
					
					'comprende_nombres','comprende_copia','asocia_ideas_sopa','asocia_ideas_agua','asocia_ideas_esc_piz',
						'asocia_ideas_lava','asocia_ideas_viaje','forma_lee','lee_palabras_silabas','silabas_trabadas',
						'lee_errores_sustitucion','lee_errores_omision','lee_errores_rotacion','lee_errores_adicion',
						'comprende_lee',
					'corto','largo','mucho','poco','grande','pequeño','lleno','vacio','n0','n2','n3','n4','n5','n6','n7','n8','n9',
						'n10',
					'mesa_pan','casa_taza','col_cal_sal','lapiz_mesa_silla_carro','dedo_mano_pie_codo','verde_verano_prado_botella',
						'pistola_oreja_pañuelo','auto_vaso_libro_pluma','animales','frutas','prendas_vestir',
					'archivo_anamnesis',
					)
	fieldsets = (
		('Paciente',{
			'classes':('collapse',),
			'fields':('paciente','archivo_anamnesis',)
		}),
		('Historia Familiar', {
				'classes': ('collapse',),
				'fields': (
					('nombre_del_padre',
					'edad_del_padre',
					'ocupacion_del_padre',
					'antecedentes_del_padre',),
					('nombre_de_la_madre',
					'edad_de_la_madre',
					'ocupacion_de_la_madre',
					'antecedentes_de_la_madre',
					'numero_de_hijos_de_la_madre'),)
			}),
			('Familiares con problemas de Lenguaje', {
				'classes': ('collapse',),
				'fields': ('nombre_del_familiar','parentesco')
			}),
			('Antecedentes Generales', {
				'classes': ('collapse',),
				'fields': (
					('duracion_del_embarazo',
					'tipo_de_parto',
					'enfermedades_durante_embarazo'),
					('problemas_glucosa',
					'edemas',
					'fiebre_alta',
					'radiografias',
					'hemorragias'),
					'alimentacion',
					'lugar_nacimiento',
					('peso',
					'talla'),
					'anestesia',
					'vacunas_completas',
					)
			}),
			('Desarrollo General del Niño', {
				'classes': ('collapse',),
				'fields': (
					'alimentacion',
					'dificultad_tragar',
					'denticion')
			}),
			('Motricidad', {
				'classes': ('collapse',),
				'fields': (
					'cuello_firme',
					('sedentacion',
					'bipedestacion'),
					'marcha_final',
					('lateralidad_mano_come',
					'lateralidad_mano_escribe',
					'lateralidad_mano_dibuja')
				)
			}),
			('Lenguaje', {
				'classes': ('collapse',),
				'fields': (
					('primeros_sonidos',
					'balbuceo'),
					('gestos_pedir',
					'primeras_palabras',
					'palabras_aparte',
					'entiende_comunes'),
					('combinacion_palabras',
					'oraciones_completas'),
					'idioma_casa',
					'imita',
					'habla_juguetes',
					'mas_comunica',
					'factores_problema',
					'ayudas_problema'
				)
			}),
			('Audicion', {
				'classes': ('collapse',),
				'fields': (
					('reaccion_sonidos_bajos',
					'reaccion_sonidos_medios',
					'reaccion_sonidos_altos'),
					('reaccion_voz_alta',
					'reaccion_voz_baja',
					'reaccion_voz_susurro'),
					('reaccion_voz_normal',
					'reaccion_voz_anormal')
				)
			}),
			('Area Psicologica Emocional', {
				'classes': ('collapse',),
				'fields': (
					'control_esfinteres',
					('sobreproteccion',
					'introversion',
					'agresividad',
					'colaboracion'),
					('dependencia',
					'extroversion',
					'egoismo',
					'desobediencia')
				)
			}),
			('Escolaridad', {
				'classes': ('collapse',),
				'fields': (
					'jardin',
					'primaria',
					('años_repetidos',
					'porque_repetidos'),
					'aprovechamiento',
					'relaciones_profesores'
				)
			}),
			('Respiracion', {
				'classes': ('collapse',),
				'fields': (
					('tipo_de_respiracion',
					'simetria_toraxica'),
					('respiracion',
					'expiracion')
				)
			}),
			('Antecedentes Patologicos', {
				'classes': ('collapse',),
				'fields': (
					('enfermedades',
					'fiebres',
					'convulsiones',
					'traumatismos',
					'operaciones',
					'afecciones_oido',
					'otros',
					'examenes_tratamientos')
				)
			}),
			('Ficha BucoFaringeo', {
				'classes': ('collapse',),
				'fields': (
					'forma_labios',
					('frenillo_sublingual_superior',
					'frenillo_sublingual_inferior'),
					('posicion_labios',
					'tamaño_lengua'),
					('movimiento_lingual_arriba',
					'movimiento_lingual_abajo',
					'movimiento_lingual_id',
					'movimiento_lingual_i',
					'movimiento_lingual_d'),
					'vibracion_lingual_interna'
				)
			}),
			('Lenguaje Expresivo', {
				'classes': ('collapse',),
				'fields': (
					('emite_sonidos_finales',
					'emite_sonidos_silabas',
					'emite_sonidos_palabras'),
					('cadencia_lenta',
					'cadencia_normal',
					'cadencia_precipitada'),
					('ritmo_tartamudeo',
					'ritmo_regular',
					'ritmo_golpe'),
					'pausas_interrupciones',
					'pensamiento_mal_formulado',
					'carece_ideas'
				)
			}),
			('Lenguaje Comprensivo', {
				'classes': ('collapse',),
				'fields': (
					('comprende_nombres',
					'comprende_copia'),
					('asocia_ideas_sopa',
					'asocia_ideas_agua',
					'asocia_ideas_esc_piz',
					'asocia_ideas_lava',
					'asocia_ideas_viaje'),
					('forma_lee',
					'lee_palabras_silabas',
					'silabas_trabadas'),
					('lee_errores_sustitucion',
					'lee_errores_omision',
					'lee_errores_rotacion',
					'lee_errores_adicion'),
					'comprende_lee',
				)
			}),
			('Comprension de Numeros', {
				'classes': ('collapse',),
				'fields': (
					('corto',
					'largo'),
					('mucho',
					'poco'),
					('grande',
					'pequeño'),
					('lleno',
					'vacio',),
					('n0',
					'n2',
					'n3',
					'n4',
					'n5',
					'n6',
					'n7',
					'n8',
					'n9',
					'n10')
				)
			}),
			('Memoria Auditiva', {
				'classes': ('collapse',),
				'fields': (
					'mesa_pan',
					'casa_taza',
					'col_cal_sal',
					'lapiz_mesa_silla_carro',
					'dedo_mano_pie_codo',
					'verde_verano_prado_botella',
					'pistola_oreja_pañuelo',
					'auto_vaso_libro_pluma',
					'animales',
					'frutas',
					'prendas_vestir',
				)
			}),
		)

	class Meta:
		verbose_name="Formulario Anamnesis del Lenguaje"
		verbose_name_plural="Formularios Anamnesis del Lenguaje"
	actions = [descarga_formulario]
admin.site.register(Anamnesis_del_Lenguaje,AnamnesisAdmin)


def descargar_formulario(modeladmin,request,queryset):
	descargar_formulario.short_description = "Marcar Formularios a descargar"
	url = '/admin/social/' + str(queryset[0].pk)
	return redirect(url)

class SocialAdmin(admin.ModelAdmin):
	list_display=(
		'paciente',
		'archivo_social',
		)
	fieldsets = (
		('Paciente',{
			'classes':('collapse',),
			'fields':('paciente','archivo_social',)
		}),
		('Cosas buenas que hace mi Hijo', {
			'classes': ('collapse',),
			'fields': (
				'suena_nariz','utiles_aseo','baja_sube_interior','zapatos','come_solo','avisa_baño','lava_seca_manos',
					'independencia_padres','duerme_solo','respeta_turno','comunica_necesidades','juega_otros_simples',
					'juega_otros_complejas','responde_caricias','expresa_sentimientos','se_defiende','puesto_hora_trabajo',
					'comporta_bien_publico','sigue_instrucciones','comparte_pertenencias','diferencia_nino_a',
					'identifica_partes_cuerpo','reconoce_organos_reproductores','cierra_perta_bano','busca_privacidad',
					'reconoce_prendas_nino_a','preparado_rechazar_caricias_extr','lugar_apropiado_necesidades',
				)
		}),
		('Que me preocupa de mi Hijo',{
			'classes':('collapse',),
			'fields':(
				'muerde','escupe','empuja_rasguna_pellizca','golpea','hala_oreja_cabello','moja_baño','llora_grita',
				'tira_piso_grita_chilla','patea_mientras_golpea_obj','tira_golpea_puertas',
			)
		}),
		('Respondo a las necesidades de mi Hijo',{
			'classes':('collapse',),
			'fields':(
				'niño_visita_al_medico','buena_nutricion','cuida_aseo_del_cuerpo','cuida_vestimenta_acorde_edad',
				'comunicamos_amor','alabamos_por_intentar','preparados_para_aceptar_problema','participamos_excursion_actividades',
				'averigua_ayuda_adicional','escuhan_musica_agrado_niño','proveen_juguetes_materiales_pedagogicos',
				'cercioramos_que_siga_reglas','enfoque_positivo_enseñanza','decimos_lo_que_tiene_que_hacer','buen_modelo_para_hijos',
				'respetamos_niño','escuchamos_atencion','enseñamos_habilidades','ambiente_fisico_es_organizado_limpio',
				'fomentamos_independencia_del_niño','ayudamos_a_hablar_de_sus_sentimientos',
			)
		}),
	)
	class Meta:
		verbose_name="Formulario Area Social"
		verbose_name_plural="Formularios Area Social"

	actions = [descargar_formulario]
admin.site.register(Area_Social,SocialAdmin)

def descargar_formulario_habilidades(modeladmin,request,queryset):
	descargar_formulario_habilidades.short_description = "Marcar Formularios a descargar"
	url = '/admin/habilidades/' + str(queryset[0].pk)
	return redirect(url)

class HabilidadesAdmin(admin.ModelAdmin):
	list_display=(
		'paciente',
		'mano_predominante','manejo_de_tijera_linea_recta','trozado','rasgado','pegado','observacion','doblado_en_dos',
			'doblado_en_cuatro',
		'sentado','parado','arrodillado','acostado','cruza_los_brazos','levanta_los_brazos','manos_en_la_cintura',
			'manos_en_la_cadera','inclinarse_hacia_delante','inclinarse_hacia_atras','pararse_puntas_de_pies','avanza','retrocede',
		'cual_es_circulo','cual_es_cuadrado','cual_es_triangulo','cual_es_rectangulo','dame_el_rojo','dame_el_amarillo',
			'dame_el_azul','dame_el_verde','dame_el_anaranjado',
		'donde_esta_tu_cabeza','donde_esta_tu_cara','donde_esta_tu_pelo','donde_esta_tus_orejas','donde_esta_tus_ojos',
			'donde_esta_tu_lengua','donde_esta_tus_dientes','donde_esta_tu_boca','donde_esta_tu_tronco','donde_esta_tu_hombro',
			'donde_esta_tus_manos','donde_esta_tus_dedos','donde_esta_tu_cintura','donde_esta_tu_nalga','donde_esta_tu_pene',
			'donde_esta_tu_pie','donde_esta_tu_rodilla','donde_esta_tu_ombligo',
		'pon_muñeco_delante','pon_muñeco_detras','pon_muñeco_en_el_medio','pon_muñeco_al_lado','pon_muñeco_arriba',
			'pon_muñeco_abajo','pon_muñeco_afuera','pon_muñeco_adentro','pon_muñeco_en_la_esquina','dia','noche','lejos','cerca',
			'rapido','lento','derecha','izquierda',
		'archivo_habilidades',
	)
	fieldsets=(
		('Paciente',{
			'classes':('collapse',),
			'fields':('paciente','archivo_habilidades',)
		}),
		('Motricidad Fina',{
			'classes':('collapse',),
			'fields':(
				'mano_predominante',('manejo_de_tijera_linea_recta','trozado','rasgado','pegado'),'observacion',('doblado_en_dos',
					'doblado_en_cuatro'),
			)
		}),
		('Motricidad Gruesa',{
			'classes':('collapse',),
			'fields':(
				('sentado','parado','arrodillado','acostado','cruza_los_brazos','levanta_los_brazos','manos_en_la_cintura',
				'manos_en_la_cadera','inclinarse_hacia_delante','inclinarse_hacia_atras','pararse_puntas_de_pies','avanza',
				'retrocede',),
			)
		}),
		('Sensopercepcion', {
			'classes': ('collapse',),
			'fields': (
				('cual_es_circulo','cual_es_cuadrado','cual_es_triangulo','cual_es_rectangulo'),
				('dame_el_rojo','dame_el_amarillo','dame_el_azul','dame_el_verde','dame_el_anaranjado',),
			)
		}),
		('Esquema Corporal',{
			'classes':('collapse',),
			'fields':(
				('donde_esta_tu_cabeza','donde_esta_tu_cara','donde_esta_tu_pelo','donde_esta_tus_orejas','donde_esta_tus_ojos',
					'donde_esta_tu_lengua','donde_esta_tus_dientes','donde_esta_tu_boca','donde_esta_tu_tronco',
					'donde_esta_tu_hombro','donde_esta_tus_manos','donde_esta_tus_dedos','donde_esta_tu_cintura',
					'donde_esta_tu_nalga','donde_esta_tu_pene','donde_esta_tu_pie','donde_esta_tu_rodilla','donde_esta_tu_ombligo',),
			)
		}),
		('Orientacion Espacial',{
			'classes':('collapse',),
			'fields':(
				('pon_muñeco_delante','pon_muñeco_detras','pon_muñeco_en_el_medio','pon_muñeco_al_lado','pon_muñeco_arriba',
					'pon_muñeco_abajo','pon_muñeco_afuera','pon_muñeco_adentro','pon_muñeco_en_la_esquina'),
					('dia','noche','lejos','cerca','rapido','lento'),('derecha','izquierda'),
			)
		}),
	)
	class Meta:
		verbose_name="Formulario Habilidades Basicas"
		verbose_name_plural="Formularios Habilidades Basicas"
	actions = [descargar_formulario_habilidades]
admin.site.register(Habilidades_Basicas,HabilidadesAdmin)

class PacienteAdmin(admin.ModelAdmin):
	list_display=('apellidos','nombres','direccion','telefono')
	fields=(('apellidos','nombres'),('direccion','telefono'),'fnac','evaluador')
	search_fields= ['apellidos','nombres','fnac','evaluador']

admin.site.register(Paciente, PacienteAdmin)

class DiscapacidadAdmin(admin.ModelAdmin):
	list_display=('paciente','motora','motora_file','auditiva','auditiva_file','visual','visual_file')
	fields = (('paciente',),('motora','motora_file',),('auditiva','auditiva_file',),('visual','visual_file',))
	search_fields=['paciente','motora','visual','auditiva']

admin.site.register(Discapacidad, DiscapacidadAdmin)


class Hilarte(admin.AdminSite):
	admin.site.site_header = 'Sistema de Registro de Terapias'
	admin.site.index_title = 'Administracion de Pacientes y Terapias'

