from django.db import models
from django.utils import timezone
from Paciente.models import Paciente
# Create your models here.

class Anamnesis_del_Lenguaje(models.Model):
#class Historia_Familiar(models.Model):
	paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
	fecha_formulario = models.DateTimeField(auto_now_add=True)
	archivo_anamnesis = models.FileField(upload_to='pruebas/anamnesis/')

	nombre_del_padre = models.CharField(max_length=50)
	edad_del_padre = models.CharField(max_length=3)
	ocupacion_del_padre = models.CharField(max_length=30)
	antecedentes_del_padre = models.TextField()
	nombre_de_la_madre = models.CharField(max_length=50)
	edad_de_la_madre = models.CharField(max_length=3)
	ocupacion_de_la_madre = models.CharField(max_length=30)
	antecedentes_de_la_madre = models.TextField()
	numero_de_hijos_de_la_madre = models.PositiveIntegerField()

#class Problemas_Familiares_de_Lenguaje(models.Model):

	nombre_del_familiar=models.CharField(max_length=50)
	parentesco = models.CharField(max_length=50)

#class Antecedentes_Generales(models.Model):
	duracion_del_embarazo = models.SmallIntegerField()
	Normal = 'NR'
	Cesarea = 'CS'
	tipo_parto_op=(
		(Cesarea,'Cesarea'),
		(Normal,'Normal'),
	)
	tipo_de_parto = models.CharField(max_length=2,choices=tipo_parto_op,default=Normal)
	enfermedades_durante_embarazo=models.TextField()
	SI='SI'
	NO='NO'
	bool_op=(
		(SI,'SI'),
		(NO,'NO'),
	)
	problemas_glucosa=models.CharField(max_length=2,choices=bool_op,default=NO)
	edemas=models.CharField(max_length=2,choices=bool_op,default=NO)
	fiebre_alta=models.CharField(max_length=2,choices=bool_op,default=NO)
	radiografias=models.CharField(max_length=2,choices=bool_op,default=NO)
	hemorragias=models.CharField(max_length=2,choices=bool_op,default=NO)
	alimentacion=models.CharField(max_length=2,choices=bool_op,default=NO)
	lugar_nacimiento=models.CharField(max_length=40,default='Guayaquil')
	peso=models.DecimalField(max_digits=4,decimal_places=2)
	talla=models.DecimalField(max_digits=4,decimal_places=2)
	anestesia=models.CharField(max_length=2,choices=bool_op,default=NO)
	vacunas_completas=models.CharField(max_length=2,choices=bool_op,default=NO)
#class desarrollo_General_Niño(models.Model):
	SENO='SENO'
	BIBERON='BIBERON'
	alimen_op=(
		(SENO,'SENO'),
		(BIBERON,'BIBERON')
	)
	alimentacion=models.CharField(max_length=7,choices=alimen_op)
	dificultad_tragar=models.CharField(max_length=2,choices=bool_op)
	denticion=models.CharField(max_length=2,choices=bool_op)

#class Motricidad(models.Model):
	cuello_firme=models.CharField(max_length=2,choices=bool_op)
	sedentacion=models.CharField(max_length=2,choices=bool_op)
	bipedestacion=models.CharField(max_length=2,choices=bool_op)
	marcha_final=models.CharField(max_length=2,choices=bool_op)
	IZQUIERDA='IZQUIERDA'
	DERECHA='DERECHA'
	mano_op=(
		(DERECHA,'DERECHA'),
		(IZQUIERDA,'IZQUIERDA')
	)
	lateralidad_mano_come=models.CharField(max_length=9,choices=mano_op)
	lateralidad_mano_escribe=models.CharField(max_length=9,choices=mano_op)
	lateralidad_mano_dibuja=models.CharField(max_length=9,choices=mano_op)
#class Lenguaje(models.Model):
	primeros_sonidos=models.CharField(max_length=40)
	balbuceo=models.CharField(max_length=40)
	gestos_pedir=models.CharField(max_length=2,choices=bool_op)
	primeras_palabras=models.CharField(max_length=40)
	palabras_aparte=models.CharField(max_length=2,choices=bool_op)
	entiende_comunes=models.CharField(max_length=2,choices=bool_op)
	combinacion_palabras=models.CharField(max_length=2,choices=bool_op)
	oraciones_completas=models.CharField(max_length=2,choices=bool_op)
	idioma_casa=models.CharField(max_length=15, default='Español')
	imita=models.CharField(max_length=4,default='mamá')
	habla_juguetes=models.CharField(max_length=2,choices=bool_op)
	mas_comunica=models.CharField(max_length=20)
	factores_problema =models.CharField(max_length=20)
	ayudas_problema=models.CharField(max_length=20)
	
#class Audicion(models.Model):
	BUENA='BUENA'
	REGULAR='REGULAR'
	MALA='MALA'
	niveles_op=(
		(BUENA,'BUENA'),
		(REGULAR,'REGULAR'),
		(MALA,'MALA')
	)
	reaccion_sonidos_bajos=models.CharField(max_length=7,choices=niveles_op)
	reaccion_sonidos_medios=models.CharField(max_length=7,choices=niveles_op)
	reaccion_sonidos_altos=models.CharField(max_length=7,choices=niveles_op)
	reaccion_voz_alta=models.CharField(max_length=7,choices=niveles_op)
	reaccion_voz_baja=models.CharField(max_length=7,choices=niveles_op)
	reaccion_voz_susurro=models.CharField(max_length=7,choices=niveles_op)
	reaccion_voz_normal=models.CharField(max_length=7,choices=niveles_op)
	reaccion_voz_anormal=models.CharField(max_length=7,choices=niveles_op)
	
#class Psicologica_Emocional(models.Model):
	control_esfinteres=models.CharField(max_length=2,choices=bool_op)
	sobreproteccion=models.CharField(max_length=2,choices=bool_op)
	introversion=models.CharField(max_length=2,choices=bool_op)
	agresividad=models.CharField(max_length=2,choices=bool_op)
	colaboracion=models.CharField(max_length=2,choices=bool_op)
	dependencia=models.CharField(max_length=2,choices=bool_op)
	extroversion=models.CharField(max_length=2,choices=bool_op)
	egoismo=models.CharField(max_length=2,choices=bool_op)
	desobediencia=models.CharField(max_length=2,choices=bool_op)

#class Escolaridad(models.Model):
	jardin=models.CharField(max_length=20)
	primaria=models.CharField(max_length=20)
	años_repetidos=models.CharField(max_length=2,choices=bool_op)
	porque_repetidos=models.CharField(max_length=50)
	aprovechamiento=models.DecimalField(max_digits=4,decimal_places=2)
	relaciones_profesores=models.CharField(max_length=8,choices=niveles_op)

#class Respiracion(models.Model):
	COSTALS='COSTAL SUPERIOR'
	COSTALD='COSTAL DIAFRAGMATICO'
	ABDOMINAL='ABDOMINAL'
	resp_op=(
		(COSTALS,'COSTAL SUPERIOR'),
		(COSTALD,'COSTAL DIAFRAGMATICO'),
		(ABDOMINAL,'ABDOMINAL')
	)
	HIPO='HIPOTONICO'
	HIPER='HIPERTONICO'
	NORMAL='NORMAL'
	simetria_op=(
		(HIPO,'HIPOTONICO'),
		(HIPER,'HIPERTONICO'),
		(NORMAL,'NORMAL')
	)
	NASAL='NASAL'
	BUCAL='BUCAL'
	forma_op=(
		(NASAL,'NASAL'),
		(BUCAL,'BUCAL')
	)
	tipo_de_respiracion=models.CharField(max_length=20,choices=resp_op)
	simetria_toraxica=models.CharField(max_length=11,choices=simetria_op)
	respiracion=models.CharField(max_length=5,choices=forma_op)
	expiracion=models.CharField(max_length=5,choices=forma_op)
	
#class Antecedentes_Patologia(models.Model):
	enfermedades=models.TextField()
	fiebres=models.CharField(max_length=2,choices=bool_op)
	convulsiones=models.CharField(max_length=2,choices=bool_op)
	traumatismos=models.CharField(max_length=2,choices=bool_op)
	operaciones=models.CharField(max_length=2,choices=bool_op)
	afecciones_oido=models.CharField(max_length=2,choices=bool_op)
	otros=models.TextField()
	examenes_tratamientos=models.TextField()

#class Ficha_BucoFaringeo(models.Model):
	NORMAL='NORMAL'
	CORTO='CORTO'
	FISURADO='FISURADO'
	COLGANTE='COLGANTE'
	labios_op=(
		(NORMAL,'NORMAL'),
		(CORTO,'CORTO'),
		(FISURADO,'FISURADO'),
		(COLGANTE,'COLGANTE')
	)
	NORMAL='NORMAL'
	RIGIDO='RIGIDO'
	pos_lab_op = (
		(NORMAL,'NORMAL'),
		(RIGIDO,'RIGIDO')
	)
	forma_labios=models.CharField(max_length=8,choices=labios_op)
	frenillo_sublingual_superior=models.CharField(max_length=2,choices=bool_op)
	frenillo_sublingual_inferior=models.CharField(max_length=2,choices=bool_op)
	posicion_labios=models.CharField(max_length=8,choices=pos_lab_op)
	MICROGLOSIA='MICROGLOSIA'
	MACROGLOSIA='MACROGLOSIA'
	NORMAL='NORMAL'
	tam_lengua_op=(
		(MICROGLOSIA,'MICROGLOSIA'),
		(NORMAL,'NORMAL'),
		(MACROGLOSIA,'MACROGLOSIA')
	)
	tamaño_lengua=models.CharField(max_length=11,choices=tam_lengua_op)
	movimiento_lingual_arriba=models.CharField(max_length=2,choices=bool_op)
	movimiento_lingual_abajo=models.CharField(max_length=2,choices=bool_op)
	movimiento_lingual_id=models.CharField(max_length=2,choices=bool_op)
	movimiento_lingual_i=models.CharField(max_length=2,choices=bool_op)
	movimiento_lingual_d=models.CharField(max_length=2,choices=bool_op)
	vibracion_lingual_interna=models.CharField(max_length=2,choices=bool_op)

#class Lenguaje_Expresivo(models.Model):
	emite_sonidos_finales=models.CharField(max_length=2,choices=bool_op)
	emite_sonidos_silabas=models.CharField(max_length=2,choices=bool_op)
	emite_sonidos_palabras=models.CharField(max_length=2,choices=bool_op)
	cadencia_lenta=models.CharField(max_length=2,choices=bool_op)
	cadencia_normal=models.CharField(max_length=2,choices=bool_op)
	cadencia_precipitada=models.CharField(max_length=2,choices=bool_op)
	ritmo_tartamudeo=models.CharField(max_length=2,choices=bool_op)
	ritmo_regular=models.CharField(max_length=2,choices=bool_op)
	ritmo_golpe=models.CharField(max_length=2,choices=bool_op)
	pausas_interrupciones=models.CharField(max_length=2,choices=bool_op)
	pensamiento_mal_formulado=models.CharField(max_length=2,choices=bool_op)
	carece_ideas=models.CharField(max_length=2,choices=bool_op)

#class Lenguaje_Comprensivo(models.Model):
	comprende_nombres=models.CharField(max_length=2,choices=bool_op)
	comprende_copia=models.CharField(max_length=2,choices=bool_op)
	asocia_ideas_sopa=models.CharField(max_length=2,choices=bool_op)
	asocia_ideas_agua=models.CharField(max_length=2,choices=bool_op)
	asocia_ideas_esc_piz=models.CharField(max_length=2,choices=bool_op)
	asocia_ideas_lava=models.CharField(max_length=2,choices=bool_op)
	asocia_ideas_viaje=models.CharField(max_length=2,choices=bool_op)
	SILABICA='SILABICA'
	VACILANTE='VACILANTE'
	CORRIENTE='CORRIENTE'
	NOLEE='NO LEE'
	lectura_op=(
		(SILABICA,'SILABICA'),
		(VACILANTE,'VACILANTE'),
		(CORRIENTE,'CORRIENTE'),
		(NOLEE,'NO LEE')
	)
	forma_lee=models.CharField(max_length=9,choices=bool_op)
	DIRECTA='DIRECTA'
	INVERSA='INVERSA'
	COMPLEJA='COMPLEJA'
	DIPTONGO='DIPTONGO'
	silabas_op=(
		(DIRECTA,'DIRECTA'),
		(INVERSA,'INVERSA'),
		(COMPLEJA,'COMPLEJA'),
		(DIPTONGO,'DIPTONGO')
	)
	lee_palabras_silabas=models.CharField(max_length=9,choices=bool_op)
	silabas_trabadas=models.CharField(max_length=2,choices=bool_op)
	lee_errores_sustitucion=models.CharField(max_length=2,choices=bool_op)
	lee_errores_omision=models.CharField(max_length=2,choices=bool_op)
	lee_errores_rotacion=models.CharField(max_length=2,choices=bool_op)
	lee_errores_adicion=models.CharField(max_length=2,choices=bool_op)
	comprende_lee=models.CharField(max_length=2,choices=bool_op)
	
#class comprension_numeros(models.Model):
	corto=models.CharField(max_length=2,choices=bool_op)
	largo=models.CharField(max_length=2,choices=bool_op)
	mucho=models.CharField(max_length=2,choices=bool_op)
	poco=models.CharField(max_length=2,choices=bool_op)
	grande=models.CharField(max_length=2,choices=bool_op)
	pequeño=models.CharField(max_length=2,choices=bool_op)
	lleno=models.CharField(max_length=2,choices=bool_op)
	vacio=models.CharField(max_length=2,choices=bool_op)
	n0=models.CharField(max_length=2,choices=bool_op)
	n2=models.CharField(max_length=2,choices=bool_op)
	n3=models.CharField(max_length=2,choices=bool_op)
	n4=models.CharField(max_length=2,choices=bool_op)
	n5=models.CharField(max_length=2,choices=bool_op)
	n6=models.CharField(max_length=2,choices=bool_op)
	n7=models.CharField(max_length=2,choices=bool_op)
	n8=models.CharField(max_length=2,choices=bool_op)
	n9=models.CharField(max_length=2,choices=bool_op)
	n10=models.CharField(max_length=2,choices=bool_op)
	
#class Memoria_Auditiva(models.Model):
	mesa_pan=models.CharField(max_length=2,choices=bool_op)
	casa_taza=models.CharField(max_length=2,choices=bool_op)
	col_cal_sal=models.CharField(max_length=2,choices=bool_op)
	lapiz_mesa_silla_carro=models.CharField(max_length=2,choices=bool_op)
	dedo_mano_pie_codo=models.CharField(max_length=2,choices=bool_op)
	verde_verano_prado_botella=models.CharField(max_length=2,choices=bool_op)
	pistola_oreja_pañuelo=models.CharField(max_length=2,choices=bool_op)
	auto_vaso_libro_pluma=models.CharField(max_length=2,choices=bool_op)
	animales=models.CharField(max_length=2,choices=bool_op)
	frutas=models.CharField(max_length=2,choices=bool_op)
	prendas_vestir=models.CharField(max_length=2,choices=bool_op)

	class Meta:
		verbose_name_plural = 'Anamnesis del Lenguaje'

class Diagnostico(models.Model):
	paciente=models.ForeignKey(Paciente, on_delete=models.CASCADE)
	presuntivo=models.TextField()
	protocolo_terapia=models.TextField()