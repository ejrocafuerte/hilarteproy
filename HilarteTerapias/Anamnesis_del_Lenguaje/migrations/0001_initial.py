# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-08 21:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Paciente', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Anamnesis_del_Lenguaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_formulario', models.DateTimeField(auto_now_add=True)),
                ('archivo_anamnesis', models.FileField(upload_to='pruebas/<django.db.models.fields.related.ForeignKey>/')),
                ('nombre_del_padre', models.CharField(max_length=50)),
                ('edad_del_padre', models.CharField(max_length=3)),
                ('ocupacion_del_padre', models.CharField(max_length=30)),
                ('antecedentes_del_padre', models.TextField()),
                ('nombre_de_la_madre', models.CharField(max_length=50)),
                ('edad_de_la_madre', models.CharField(max_length=3)),
                ('ocupacion_de_la_madre', models.CharField(max_length=30)),
                ('antecedentes_de_la_madre', models.TextField()),
                ('numero_de_hijos_de_la_madre', models.PositiveIntegerField()),
                ('nombre_del_familiar', models.CharField(max_length=50)),
                ('parentesco', models.CharField(max_length=50)),
                ('duracion_del_embarazo', models.SmallIntegerField()),
                ('tipo_de_parto', models.CharField(choices=[('CS', 'Cesarea'), ('NR', 'Normal')], default='NR', max_length=2)),
                ('enfermedades_durante_embarazo', models.TextField()),
                ('problemas_glucosa', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('edemas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('fiebre_alta', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('radiografias', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('hemorragias', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('lugar_nacimiento', models.CharField(default='Guayaquil', max_length=40)),
                ('peso', models.DecimalField(decimal_places=2, max_digits=4)),
                ('talla', models.DecimalField(decimal_places=2, max_digits=4)),
                ('anestesia', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('vacunas_completas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], default='NO', max_length=2)),
                ('alimentacion', models.CharField(choices=[('SENO', 'SENO'), ('BIBERON', 'BIBERON')], max_length=7)),
                ('dificultad_tragar', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('denticion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cuello_firme', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('sedentacion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('bipedestacion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('marcha_final', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lateralidad_mano_come', models.CharField(choices=[('DERECHA', 'DERECHA'), ('IZQUIERDA', 'IZQUIERDA')], max_length=9)),
                ('lateralidad_mano_escribe', models.CharField(choices=[('DERECHA', 'DERECHA'), ('IZQUIERDA', 'IZQUIERDA')], max_length=9)),
                ('lateralidad_mano_dibuja', models.CharField(choices=[('DERECHA', 'DERECHA'), ('IZQUIERDA', 'IZQUIERDA')], max_length=9)),
                ('primeros_sonidos', models.CharField(max_length=40)),
                ('balbuceo', models.CharField(max_length=40)),
                ('gestos_pedir', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('primeras_palabras', models.CharField(max_length=40)),
                ('palabras_aparte', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('entiende_comunes', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('combinacion_palabras', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('oraciones_completas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('idioma_casa', models.CharField(default='Español', max_length=15)),
                ('imita', models.CharField(default='mamá', max_length=4)),
                ('habla_juguetes', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('mas_comunica', models.CharField(max_length=20)),
                ('factores_problema', models.CharField(max_length=20)),
                ('ayudas_problema', models.CharField(max_length=20)),
                ('reaccion_sonidos_bajos', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_sonidos_medios', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_sonidos_altos', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_voz_alta', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_voz_baja', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_voz_susurro', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_voz_normal', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('reaccion_voz_anormal', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=7)),
                ('control_esfinteres', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('sobreproteccion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('introversion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('agresividad', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('colaboracion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('dependencia', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('extroversion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('egoismo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('desobediencia', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('jardin', models.CharField(max_length=20)),
                ('primaria', models.CharField(max_length=20)),
                ('años_repetidos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('porque_repetidos', models.CharField(max_length=50)),
                ('aprovechamiento', models.DecimalField(decimal_places=2, max_digits=4)),
                ('relaciones_profesores', models.CharField(choices=[('BUENA', 'BUENA'), ('REGULAR', 'REGULAR'), ('MALA', 'MALA')], max_length=8)),
                ('tipo_de_respiracion', models.CharField(choices=[('COSTAL SUPERIOR', 'COSTAL SUPERIOR'), ('COSTAL DIAFRAGMATICO', 'COSTAL DIAFRAGMATICO'), ('ABDOMINAL', 'ABDOMINAL')], max_length=20)),
                ('simetria_toraxica', models.CharField(choices=[('HIPOTONICO', 'HIPOTONICO'), ('HIPERTONICO', 'HIPERTONICO'), ('NORMAL', 'NORMAL')], max_length=11)),
                ('respiracion', models.CharField(choices=[('NASAL', 'NASAL'), ('BUCAL', 'BUCAL')], max_length=5)),
                ('expiracion', models.CharField(choices=[('NASAL', 'NASAL'), ('BUCAL', 'BUCAL')], max_length=5)),
                ('enfermedades', models.TextField()),
                ('fiebres', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('convulsiones', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('traumatismos', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('operaciones', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('afecciones_oido', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('otros', models.TextField()),
                ('examenes_tratamientos', models.TextField()),
                ('forma_labios', models.CharField(choices=[('NORMAL', 'NORMAL'), ('CORTO', 'CORTO'), ('FISURADO', 'FISURADO'), ('COLGANTE', 'COLGANTE')], max_length=8)),
                ('frenillo_sublingual_superior', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('frenillo_sublingual_inferior', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('posicion_labios', models.CharField(choices=[('NORMAL', 'NORMAL'), ('RIGIDO', 'RIGIDO')], max_length=8)),
                ('tamaño_lengua', models.CharField(choices=[('MICROGLOSIA', 'MICROGLOSIA'), ('NORMAL', 'NORMAL'), ('MACROGLOSIA', 'MACROGLOSIA')], max_length=11)),
                ('movimiento_lingual_arriba', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('movimiento_lingual_abajo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('movimiento_lingual_id', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('movimiento_lingual_i', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('movimiento_lingual_d', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('vibracion_lingual_interna', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('emite_sonidos_finales', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('emite_sonidos_silabas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('emite_sonidos_palabras', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cadencia_lenta', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cadencia_normal', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('cadencia_precipitada', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('ritmo_tartamudeo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('ritmo_regular', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('ritmo_golpe', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('pausas_interrupciones', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('pensamiento_mal_formulado', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('carece_ideas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comprende_nombres', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comprende_copia', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('asocia_ideas_sopa', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('asocia_ideas_agua', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('asocia_ideas_esc_piz', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('asocia_ideas_lava', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('asocia_ideas_viaje', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('forma_lee', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=9)),
                ('lee_palabras_silabas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=9)),
                ('silabas_trabadas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lee_errores_sustitucion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lee_errores_omision', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lee_errores_rotacion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lee_errores_adicion', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('comprende_lee', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('corto', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('largo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('mucho', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('poco', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('grande', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('pequeño', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lleno', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('vacio', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n0', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n2', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n3', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n4', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n5', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n6', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n7', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n8', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n9', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('n10', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('mesa_pan', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('casa_taza', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('col_cal_sal', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('lapiz_mesa_silla_carro', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('dedo_mano_pie_codo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('verde_verano_prado_botella', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('pistola_oreja_pañuelo', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('auto_vaso_libro_pluma', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('animales', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('frutas', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('prendas_vestir', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=2)),
                ('paciente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Paciente.Paciente')),
            ],
            options={
                'verbose_name_plural': 'Anamnesis del Lenguaje',
            },
        ),
        migrations.CreateModel(
            name='Diagnostico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('presuntivo', models.TextField()),
                ('protocolo_terapia', models.TextField()),
                ('paciente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Paciente.Paciente')),
            ],
        ),
    ]
