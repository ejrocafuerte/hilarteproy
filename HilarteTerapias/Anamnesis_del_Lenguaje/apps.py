from django.apps import AppConfig


class TerapiasConfig(AppConfig):
    name = 'Terapias'
